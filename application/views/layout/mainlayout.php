<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>    
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>

</head>
<body>
<?php
	$this->load->view('layout/header');
	$this->load->view($page);
	$this->load->view('layout/footer');
?>


</body>
</html>